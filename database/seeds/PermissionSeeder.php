<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addDeleteUsers = new Permission();
        $addDeleteUsers->name = 'add delete users';
        $addDeleteUsers->slug = 'add-delete-users';
        $addDeleteUsers->save();

        $addDeleteBooks = new Permission();
        $addDeleteBooks->name = 'Add delete books';
        $addDeleteBooks->slug = 'add-delete-books';
        $addDeleteBooks->save();


        $reserveBooks = new Permission();
        $reserveBooks->name = 'Reserve Books';
        $reserveBooks->slug = 'reserve-books';
        $reserveBooks->save();





    }
}
