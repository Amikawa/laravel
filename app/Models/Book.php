<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable
        = [
            'name', 'code', 'category_id', 'description', 'image', 'count',
        ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeByCode($query, $code)
    {
        return $query->where('code', $code);
    }
}
