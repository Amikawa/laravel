<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'name' => 'Гарри Поттер',
                'code' => 'potter',
                'description' => 'Отличный фентези про мальчика, учащегося в волшебной школе',
                'category_id' => 1,
                'image' => 'https://upload.wikimedia.org/wikipedia/ru/b/b4/Harry_Potter_and_the_Philosopher%27s_Stone_%E2%80%94_movie.jpg',
            ],
            [
                'name' => 'Война и мир',
                'code' => 'voina_i_mir',
                'description' => 'Шедевр Льва Толстого в четыре тома',
                'category_id' => 2,
                'image' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/War-and-peace_1873.gif/269px-War-and-peace_1873.gif',
            ],
            [
                'name' => 'Наруто.Ураганные хроники.',
                'code' => 'naruto',
                'description' => 'Легендарная манга про мальчика с демоном внутри',
                'category_id' => 3,
                'image' => 'https://gen.jut.su/templates/school/images/manga_naruto.jpg',
            ],
            [
                'name' => 'Тетрадь смерти',
                'code' => 'death_note',
                'description' => 'Хороший детектив, где главный герой получает тетрадь смерти..',
                'category_id' => 3,
                'image' => 'https://avatars.mds.yandex.net/get-kinopoisk-image/1600647/5b7079b7-85e7-4bcd-ace1-a132e92a72f4/360',
            ],
            [
                'name' => 'Хроники Дарьи Донцовой',
                'code' => 'darya',
                'description' => 'Детeктив Дарьи Донцовой..',
                'category_id' => 1,
                'image' => '',
            ],
            [
                'name' => 'Капитанская дочь',
                'code' => 'capitan',
                'description' => 'Одно из произведений А.С. Пушкина.',
                'category_id' => 3,
                'image' => '',
            ],
            [
                'name' => 'Капитанская дочь',
                'code' => 'Loremds',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore !',

                'category_id' => 1,
                'image' => '',
            ],
            [
                'name' => 'Капитанская дочь',
                'code' => 'Loremd',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore!',

                'category_id' => 2,
                'image' => '',
            ],
            [
                'name' => 'Капитанская дочь',
                'code' => 'Lorem',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',

                'category_id' => 3,
                'image' => '',
            ],
            [
                'name' => 'Капитанская дочь',
                'code' => 'Loremdsf',
                'description' => 'Для самых смелых идей',

                'category_id' => 2,
                'image' => '',
            ],
            [
                'name' => 'Капитанская дочь',
                'code' => 'Loremhbgfg',
                'description' => 'Любите домашние котлеты? Вам определенно стоит посмотреть на эту мясорубку!',

                'category_id' => 1,
                'image' => '',
            ],
        ]);
    }
}
