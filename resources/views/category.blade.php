@extends('layouts.master')

@section('title', 'Категория ' . $category->name)

@section('content')
    <h1>
        {{$category->name}} - Всего в библиотеке {{ $category->books->count() }} книг.
    </h1>
    <p>
        {{ $category->description }}
    </p>
    <div class="row">
        @foreach($category->books as $book)
            @include('layouts.card', compact('book'))
        @endforeach
    </div>
@endsection
