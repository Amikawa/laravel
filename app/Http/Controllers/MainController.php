<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Book;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index() {
        $books = Book::get();
        return view('index', compact('books'));
    }

    public function categories() {
        $categories = Category::get();
        return view('categories', compact('categories'));
    }

    public function category($code) {
        $category = Category::where('code', $code)->first();
        return view('category', compact('category'));
    }

    public function book($categoryCode, $bookCode ) {
        $book = Book::where('code',$bookCode)->first();
        return view('book', ['book' => $book]);
    }
}
