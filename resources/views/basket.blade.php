@extends('layouts.master')

@section('title', 'Корзина')

@section('content')
    <h1>Корзина</h1>
    <p>Оформление брони</p>
    <div class="panel">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Название</th>
                <th>Кол-во</th>
                <th>Описание</th>
                <th>Осталось книг</th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->books as $book)
                <tr>
                    <td>
                        <a href="{{ route('book', [$book->category->code, $book->code]) }}">
                            <img height="56px" src="http://laravel-diplom-1.rdavydov.ru/storage/books/iphone_x.jpg">
                            {{ $book->name }}
                        </a>
                    </td>
                    <td><span class="badge">{{ $book->pivot->count }}</span>
                        <div class="btn-group form-inline">
                            <form action="{{ route('basket-remove', $book) }}" method="POST">
                                <button type="submit" class="btn btn-danger"
                                        href=""><span
                                        class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
                                @csrf
                            </form>
                            <form action="{{ route('basket-add', $book) }}" method="POST">
                                <button type="submit" class="btn btn-success"
                                        href=""><span
                                        class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                @csrf
                            </form>
                        </div>
                    </td>
                    <td>{{ $book->description }} руб.</td>
                    <td>{{ $book->total }}.</td>
                </tr>
            @endforeach

            </tbody>
        </table>
        <br>
        <div class="btn-group pull-right" role="group">
            <a type="button" class="btn btn-success" href="{{ route('basket-place') }}">Оформить
                бронь</a>
        </div>
    </div>
@endsection
