@extends('layouts.master')

@section('title', 'Книга')

@section('content')
    <h1>{{$book->name}}</h1>
    <img src="{{ $book->image }}">
    <p>Количество: <b>{{$book->total}}.</b></p>

    <p>{{$book->description}}</p>
    <form action="{{route('basket-add',['book' => $book,])}}" method="POST">
        @csrf
        <button  type="submit">Добавить в корзину</button>
    </form>
@endsection
