@extends('layouts.master')

@section('title', 'Главная')

@section('content')
    <h1>Все книги</h1>

    <div class="row">
        @foreach($books as $book)
            @include('layouts.card', compact('book'))
        @endforeach
    </div>
@endsection
