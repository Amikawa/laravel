<?php

use App\Models\Permission;
use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('slug','admin')->first();
        $librarian = Role::where('slug', 'librarian')->first();
        $user = Role::where('slug','user')->first();

        $createUsers = Permission::where('slug','add-delete-users')->first();
        $createBooks = Permission::where('slug','add-delete-books')->first();
        $reserveBooks = Permission::where('slug','reserve-books')->first();


        $user1 = new User();
        $user1->name = 'Jhon Deo';
        $user1->email = 'jhon@deo.com';
        $user1->password = bcrypt('secret');
        $user1->save();
        $user1->roles()->attach($admin);
        $user1->permissions()->attach($createUsers);

        $user2 = new User();
        $user2->name = 'Mike Thomas';
        $user2->email = 'mike@thomas.com';
        $user2->password = bcrypt('secret');
        $user2->save();
        $user2->roles()->attach($librarian);
        $user2->permissions()->attach($createBooks);

        $user3 = new User();
        $user3->name = 'Vitya Pupkin';
        $user3->email = 'Vitjaaa@mail.com';
        $user3->password = bcrypt('secret');
        $user3->save();
        $user3->roles()->attach($user);
        $user3->permissions()->attach($reserveBooks);
    }
}
