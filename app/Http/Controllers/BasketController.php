<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Book;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    public function basket()
    {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
        }
        return view('basket', compact('order'));
    }

    public function basketConfirm(Request $request)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('index');
        }
        $order = Order::find($orderId);

        foreach($order->books as $bookEach)
        {   dump('hi');
            $count = $bookEach->pivot->count;
//            dump($bookEach->pivot);
//            dump($bookEach->pivot->count);

            $bookEach->reserved += $count;
            $bookEach->save();
        }
        $success = $order->saveOrder($request->name, $request->phone);

        if ($success) {
            session()->flash('success', 'Ваш заказ принят в обработку!');
        } else {
            session()->flash('warning', 'Случилась ошибка');
        }


        return redirect()->route('index');
    }

    public function basketPlace()
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('index');
        }
        $order = Order::find($orderId);
        return view('order', compact('order'));
    }

    public function basketAdd($bookId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            $order = Order::create();
            session(['orderId' => $order->id]);
        } else {
            $order = Order::find($orderId);
        }

        if ($order->books->contains($bookId)) {
            $pivotRow = $order->books()->where('book_id', $bookId)->first()->pivot;
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            $order->books()->attach($bookId);
        }

        $book = Book::find($bookId);

        session()->flash('success', 'Добавлена книга ' . $book->name);

        return redirect()->route('basket');
    }

    public function basketRemove($bookId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('basket');
        }
        $order = Order::find($orderId);


            $pivotRow = $order->books()->where('book_id', $bookId)->first()->pivot;
            if ($pivotRow->count < 2) {
                $order->books()->detach($bookId);
            } else {
                $pivotRow->count--;
                $pivotRow->update();

        }

        $book = Book::find($bookId);

        session()->flash('warning', 'Удалена книга  ' . $book->name);

        return redirect()->route('basket');
    }
}
