<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <img src="{{$book->image}}" alt="not loaded yet">
        <div class="caption">
            <h3>{{ $book->name }}</h3>
            <p>{{ $book->total }} штук доступно</p>
            <p>
                <form action="{{ route('basket-add', $book) }}" method="POST">
                <button type="submit" class="btn btn-primary" role="button">В корзину</button>
                <a href="{{ route('book', [$book->category->code, $book->code]) }}" class="btn btn-default"
                   role="button">Подробнее</a>
                @csrf
            </form>
            </p>
        </div>
    </div>
</div>
