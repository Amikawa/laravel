<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = 'Admin';
        $admin->slug = 'admin';
        $admin->save();

        $librarian = new Role();
        $librarian->name = 'Librarian';
        $librarian->slug = 'librarian';
        $librarian->save();

        $user = new Role();
        $user->name = 'User';
        $user->slug = 'user';
        $user->save();
    }
}
